'use strict';

var app = angular.module('app', [
    'ngRoute', 'ui.bootstrap', 'ngMap', 'fiestah.money', 'ngCookies', 'angularGrid', 'luegg.directives', 'uiCropper', 'ngFileUpload', 'directive.g+signin'
]);

// variables needed for SendBird Connection
var sb;
var appId = 'C9E36997-42B9-41AA-A80A-B6717B6BB724';
var accessToken = '29be49d67f52e60c0d6b441326d4cad6b3a32d0f';
var userId = '';
var nickname = '';
var currentUser;

app.run( function($cookies) {
    sb = new SendBird({
        appId: appId
    });
    if (typeof($cookies.get('userId')) !== 'undefined') {
        userId = $cookies.get('userId') ;
        sb.connect(userId, accessToken, function(user, error) {
            currentUser = user;
            if (error){
                console.log(error);
            }
        });
        console.log('**********************');
        console.log(sb);
        console.log($cookies.get('userId'));
    }

});

// Configuring CORS
app.config(function ($httpProvider) {
    //Enable cross domain calls
    $httpProvider.defaults.useXDomain = true;

    //Remove the header used to identify ajax call  that would prevent CORS from working
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
});

// URL Routing
app.config(['$locationProvider', '$routeProvider', function ($locationProvider, $routeProvider) {
    $locationProvider.hashPrefix('!');
    $routeProvider
        .when('/login', {
            templateUrl: 'templates/login.html'
        })
        .when('/register', {
            templateUrl: 'templates/register.html'
        })
        .when('/recover', {
            templateUrl: 'templates/password_recover.html'
        })
        .when('/home', {
            templateUrl: 'templates/home.html'
        })
        .when('/quester/:userId', {
            templateUrl: 'templates/quester_profile.html'
        })
        .when('/quester/edit/:userId', {
            templateUrl: 'templates/quester_edit_profile.html'
        })
        .when('/notifications', {
            templateUrl: 'templates/notifications.html'
        })
        .when('/search', {
            templateUrl: 'templates/quest_search.html'
        })
        .when('/chat', {
            templateUrl: 'templates/chat.html'
        })
        .when('/quests/created', {
            templateUrl: 'templates/user_created_quests.html'
        })
        .when('/quests/todo', {
            templateUrl: 'templates/users_active_quests.html'
        })
        .when('/quests/new', {
            templateUrl: 'templates/create_quest.html'
        })
        .when('/quests/:questId', {
            templateUrl: 'templates/quest_home.html'
        })
        .when('/quests/edit/:questId', {
            templateUrl: 'templates/quest_edit.html'
        })
        .when('/inventory', {
            templateUrl: 'templates/inventory.html'
        })
        .when('/admin', {
            templateUrl: 'templates/admin.html'
        })
        .when('/item/create', {
            templateUrl: 'templates/item_creation.html'
        })
        .otherwise({
            redirectTo: '/login'
        });
}]);

// Controller For Admin Page
app.controller('AdminCtrl', function ($scope, $http, $location, $cookies, $uibModal) {
    if ($cookies.get('userId') !== '30de2a50-3e93-4a83-aa13-f7192aec2f0c') {
        $location.path("/home");
    }

    $scope.reports = [];
    $scope.searchStr = '';
    $scope.reasonFilter = '';
    $scope.userIdsToNames = {};
    $scope.reportReasons = ['Bad Offline Behavior', 'Inappropriate Messages', 'Inappropriate Photos',
        'Inappropriate Quest Details', 'Other', 'Spam Or Misleading', 'ALL'];

    $scope.reportResolved = false;

    $http.get("http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/report", {
        transformRequest: angular.identity,
        headers: {
            'Content-Type': undefined
        }
    })
        .success(function (response) {
            console.log(response);
            $scope.reports = response;
        })
        .error(function (response) {
            console.log(response);
        });

    $http.get("http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/user", {
        transformRequest: angular.identity,
        headers: {
            'Content-Type': undefined
        }
    })
        .success(function (response) {
            console.log(response);
            var numUsers = response.length;
            for(var i = 0; i < numUsers; i++) {
                $scope.userIdsToNames[response[i].userId] = response[i].name;
            }
        })
        .error(function (response) {
            console.log(response);
        });

    $scope.openReport = function(report) {
        $scope.reportResolved = false;
        console.log(report);

        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: '/templates/modal_contents_report.html',
            controller: 'ModalReportCtrl',
            size: 'md',
            resolve: {
                reportInfo: function () {
                    return {
                        report: report
                    };
                }
            }
        });

        modalInstance.result.then(function () {
            $scope.reportResolved = true;

            var numReports = $scope.reports.length;
            for (var j = 0; j < numReports; j++) {
                if ($scope.reports[j].reportId === report.reportId){
                    $scope.reports[j].status = 'RESOLVED'
                }
            }

        }, function () {
            console.log('Modal dismissed at: ' + new Date());
        });
    };
});

// Factory For Report Resolve Function
app.factory('ReportResolve', ['$http', function ($http) {
    return {
        resolve: function (reportId) {
            return $http({
                method: 'put',
                url: 'http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/report/' + reportId,
                id: reportId,
                headers: {'Content-Type': 'application/json'}
            })
        },
        updateHealth: function (health, userId) {
            console.log(health);
            var data = {
                health: health
            };
            console.log(JSON.stringify(data));
            return $http({
                method: 'put',
                url: 'http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/user/' + userId,
                id: userId,
                data: JSON.stringify(data),
                headers: {'Content-Type': 'application/json'}
            })
        }
    }
}]);

// Controller For Report Viewing
app.controller('ModalReportCtrl', function ($scope, $http, $uibModalInstance,ReportResolve, reportInfo) {
    $scope.reportId = reportInfo.report.reportId;
    $scope.reporterId = reportInfo.report.reporterId;
    $scope.offenderId = reportInfo.report.offenderId;
    $scope.status = reportInfo.report.status;
    $scope.timestamp = reportInfo.report.timestamp;
    $scope.reason = reportInfo.report.reason;
    $scope.message = reportInfo.report.message;
    $scope.questId = reportInfo.report.questId;
    $scope.offenderName = '';
    $scope.offenderHealth = 0;
    $scope.reporterName = '';
    $scope.questName = '';
    $scope.hasQuest = $scope.questId !== null && $scope.questId !== '';
    $scope.inputHealthAmount = 0;
    $scope.errorResolving = false;
    $scope.errorUpdatingHealth = false;
    $scope.errorBanning = false;


    $http.get("http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/user/" + $scope.offenderId, {
        transformRequest: angular.identity,
        headers: {
            'Content-Type': undefined
        }
    })
        .success(function (response) {
            $scope.offenderName = response.name;
            $scope.offenderHealth = response.health;
        })
        .error(function (response) {
            console.log(response);
        });

    $http.get("http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/user/" + $scope.reporterId, {
        transformRequest: angular.identity,
        headers: {
            'Content-Type': undefined
        }
    })
        .success(function (response) {
            $scope.reporterName = response.name;
        })
        .error(function (response) {
            console.log(response);
        });

    if($scope.hasQuest) {
        $http.get("http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/quest/" + $scope.questId, {
            transformRequest: angular.identity,
            headers: {
                'Content-Type': undefined
            }
        })
            .success(function (response) {
                $scope.questName = response.name;
            })
            .error(function (response) {
                console.log(response);
            });
    }

    $scope.dismissReport = function  () {
        $scope.errorUpdatingHealth = false;
        $scope.errorResolving = false;
        $scope.errorBanning = false;
        ReportResolve.resolve($scope.reportId)
            .success(function (response) {
                $uibModalInstance.close();
            })
            .error(function (response) {
                if (response.statusCode === 500) {
                    $scope.errorResolving = true;
                }
                console.log(response);
            });
    };

    $scope.deductHealth = function  (health1, health2) {
        $scope.errorResolving = false;
        $scope.errorUpdatingHealth = false;
        $scope.errorBanning = false;
        console.log(health1);
        console.log(health2);
        ReportResolve.updateHealth(health1 + health2, $scope.offenderId)
            .success(function (response) {
                ReportResolve.resolve($scope.reportId)
                    .success(function (response) {
                        $uibModalInstance.close();
                    })
                    .error(function (response) {
                        if (response.statusCode === 500) {
                            $scope.errorResolving = true;
                        }
                        console.log(response);
                    });
            })
            .error(function (response) {
                if (response.statusCode === 500) {
                    $scope.errorUpdatingHealth = true;
                }
                console.log(response);
            });

    };

    $scope.banOffender = function  () {
        $scope.errorUpdatingHealth = false;
        $scope.errorResolving = false;
        $scope.errorBanning = false;

        ReportResolve.updateHealth(12, $scope.offenderId)
            .success(function (response) {
                ReportResolve.resolve($scope.reportId)
                    .success(function (response) {
                        $uibModalInstance.close();
                    })
                    .error(function (response) {
                        if (response.statusCode === 500) {
                            $scope.errorResolving = true;
                        }
                        console.log(response);
                    });
            })
            .error(function (response) {
                if (response.statusCode === 500) {
                    $scope.errorBanning = true;
                }
                console.log(response);
            });
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});

// Factory For Password Recovery Function
app.factory('PasswordRecover', ['$http', function ($http) {
    return {
        sendPass: function (email) {
            var data = {
                email: email
            };
            console.log(JSON.stringify(data));
            return $http({
                method: 'post',
                url: 'http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/user/forgot',
                data: JSON.stringify(data),
                headers: {'Content-Type': 'application/json'}
            })
        },
        updatePass: function (email, tempPassword, newPassword) {
            var data = {
                email: email,
                tempPassword: tempPassword,
                newPassword: newPassword
            };
            console.log(JSON.stringify(data));
            return $http({
                method: 'post',
                url: 'http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/user/password',
                data: JSON.stringify(data),
                headers: {'Content-Type': 'application/json'}
            })
        }
    }
}]);

// Controller For Password Recovery Page
app.controller('RecoverCtrl', function ($scope, $http, $location, PasswordRecover, $uibModal) {
    $scope.email = '';
    $scope.password = '';
    $scope.password2 = '';
    $scope.tempPassword = '';

    $scope.cantGetUser = false;

    $scope.sendTempPassword = function (event) {
        $scope.cantGetUser = false;
        if ($scope.email === '') {
            $scope.cantGetUser = true;
            return;
        }
        PasswordRecover.sendPass($scope.email)
            .success(function (response) {

                $scope.animationsEnabled = true;

                var modalInstance = $uibModal.open({
                    animation: $scope.animationsEnabled,
                    templateUrl: '/templates/modal_contents_pass_reset.html',
                    controller: 'ModalPasswordResetCtrl',
                    size: 'md',
                    resolve: {
                        userInfo: function() {
                            return {
                                email: $scope.email
                            };
                        }
                    }
                });

                modalInstance.result.then(function (response) {
                    $location.path('/login');
                }, function () {
                    console.log('Modal dismissed at: ' + new Date());
                });

            })
            .error(function (response) {
                if (response.statusCode === 500) {
                    $scope.cantGetUser = true;
                }
                console.log(response);
            });
    };
});

// Controller For Tag Creation from Quest Creation Page
app.controller('ModalPasswordResetCtrl', function ($scope, $uibModalInstance, $http, PasswordRecover, userInfo, $location) {
    $scope.tempPassword = '';
    $scope.password = '';
    $scope.password2 = '';
    $scope.email = userInfo.email;
    $scope.passwordMismatch = false;
    $scope.errorUpdating = false;

    $scope.ok = function () {
        $scope.passwordMismatch = false;
        $scope.errorUpdating = false;
        if($scope.password !== $scope.password2) {
            $scope.passwordMismatch = true;
            return;
        }

        PasswordRecover.updatePass($scope.email, $scope.tempPassword, $scope.password)
            .then(function (response) {
                console.log(response);
                $uibModalInstance.close(response);
            }, function (response) {
                $scope.errorUpdating = response.statusCode === 500;
                console.log(response);
            });
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});

// Factory For User Register Function
app.factory('Register', ['$http', function ($http) {
    return {
        create: function (data) {
            console.log(JSON.stringify(data));
            return $http({
                method: 'post',
                url: 'http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/user',
                data: JSON.stringify(data),
                headers: {'Content-Type': 'application/json'}
            })
        }
    }
}]);

// Controller For User Registration Page
app.controller('RegisterCtrl', function ($scope, $http, Register, $location, Upload) {
    $scope.name = '';
    $scope.email = '';
    $scope.password = '';
    $scope.password2 = '';
    $scope.roleId = 'afb76b4d-95bd-481e-a06d-81b64af17b97';
    $scope.profileImageId = '';
    $scope.mismatchPasswords = false;
    $scope.passwordTooShort = false;
    $scope.emailExists = false;
    $scope.cantCreateUser = false;
    $scope.profileFile = '';
    $scope.cities = [];
    $scope.city = '';

    $scope.myImage = '';
    $scope.myCroppedImage = '';

    var handleFileSelect = function(evt) {
        var file=evt.currentTarget.files[0];
        var reader = new FileReader();
        reader.onload = function (evt) {
            $scope.$apply(function($scope) {
                $scope.myImage = evt.target.result;
            });
        };
        reader.readAsDataURL(file);
    };
    angular.element(document.querySelector('#fileInput')).on('change', handleFileSelect);


    $http.get("http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/city", {
        transformRequest: angular.identity,
        headers: {
            'Content-Type': undefined
        }
    })
        .success(function (response) {
            $scope.cities = response;
        })
        .error(function (response) {
            console.log(response);
        });


    $scope.register = function (event) {
        $scope.mismatchPasswords = false;
        $scope.passwordTooShort = false;
        $scope.emailExists = false;
        $scope.cantCreateUser = false;
        if ($scope.password !== $scope.password2 || $scope.password.length < 9) {
            if ($scope.password !== $scope.password2){
                $scope.mismatchPasswords = true;
            }
            if ($scope.password.length < 9) {
                $scope.passwordTooShort = true;
            }
            return;
        }


        var binary = atob($scope.myCroppedImage.split(',')[1]);
        var mimeString = $scope.myCroppedImage.split(',')[0].split(':')[1].split(';')[0];
        var array = [];
        for(var i = 0; i < binary.length; i++) {
            array.push(binary.charCodeAt(i));
        }
        var blob = new Blob([new Uint8Array(array)], {type: mimeString});

        var theBlob = blob;
        theBlob.lastModifiedDate = new Date();
        theBlob.name = "my-image.png";

        console.log(theBlob);
        Upload.upload({
            url: 'http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/image',
            data: {file: theBlob}
        }).then(function (resp) {
            console.log('Success ' + resp + ' uploaded. Response: ' + resp);
            console.log(resp);
            $scope.profileImageId = resp.data.id;

            var data = {
                name: $scope.name,
                email: $scope.email,
                password: $scope.password,
                cityId: $scope.city.id,
                roleId: $scope.roleId,
                profileImageId: $scope.profileImageId
            };
            $scope.emailExists = false;
            $scope.cantCreateUser = false;
            event.preventDefault();
            Register.create(data)
                .then(function () {
                    $location.path("/login");
                }, function (response) {
                    if (response.data.statusCode === 409) {
                        $scope.emailExists = true;
                    }
                    if (response.data.statusCode === 500) {
                        $scope.cantCreateUser = true;
                    }
                });
        }, function (resp) {
            console.log('Error status: ' + resp.status);
        }, function (evt) {
            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
        });
    };
});

// Factory For Login Function
app.factory('UserLogin', ['$http', function ($http) {
    return {
        login: function (username, password) {
            var data = {
                email: username,
                password: password
            };
            console.log(JSON.stringify(data));
            return $http({
                method: 'post',
                url: 'http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/login',
                data: JSON.stringify(data),
                headers: {'Content-Type': 'application/json'}
            })
        },
        loginEmail: function (email, key) {
            var data = {
                email: email,
                key: key
            };
            console.log(JSON.stringify(data));
            return $http({
                method: 'post',
                url: 'http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/login/email',
                data: JSON.stringify(data),
                headers: {'Content-Type': 'application/json'}
            })
        }
    }
}]);

// Controller For Navbar
app.controller('NavbarCtrl', function ($scope, $http, $window, UserLogin, $location, $cookies) {

    if (typeof($cookies.get('userId')) === 'undefined' && typeof($cookies.get('jwt')) === 'undefined' ) {
        $location.path("/login");
    }

    $scope.username = '';
    $scope.password = '';
    $scope.isAuthenticated = false;
    $scope.invalidEmailOrPass = false;
    $scope.cantFindUser = false;
    $scope.blankFields = false;
    $scope.userId = $cookies.get('userId');

    $scope.logout = function (event) {
        console.log(event);
        $cookies.remove('userId');
        $scope.username = '';
        $scope.password = '';
        $scope.response = '';
        $scope.userId = '';
        $scope.isAuthenticated = false;
        $cookies.remove('jwt');
        sb.disconnect(function () {});
        $location.path("/");
    };
});

// Controller For User Login Page
app.controller('UserCtrl', function ($scope, $http, $window, UserLogin, $location, $cookies, Register) {

    if (typeof($cookies.get('userId')) !== 'undefined' && typeof($cookies.get('jwt')) !== 'undefined' ) {
        $location.path("/home");
    }

    console.log('login PAGE');
    $scope.username = '';
    $scope.password = '';
    $scope.isAuthenticated = false;
    $scope.invalidEmailOrPass = false;
    $scope.cantFindUser = false;
    $scope.blankFields = false;
    $scope.userId = $cookies.get('userId');

    var myImageId = '';
    var myImageLink = '';
    var myName = '';

    $scope.$on('event:google-plus-signin-success', function (event,authResult) {
        console.log(event, authResult);
        // Send login to server or save into cookie
        gapi.client.load('plus', 'v1', apiClientLoaded);

        function apiClientLoaded() {
            gapi.client.plus.people.get({userId: 'me'}).execute(handleResponse);
        }

        function handleResponse(resp) {
            console.log(resp);

            var g_email = resp.emails[0].value;
            var g_name = resp.displayName;

            console.log(g_email, g_name);


            var data = {
                name: g_name,
                email: g_email,
                cityId: 'a22b9ba1-4daa-47f0-8cb6-4fae88bb9e3a',
                roleId: 'afb76b4d-95bd-481e-a06d-81b64af17b97',
                key: 'LS0tLS1CRUdJTiBQVUJMSUMgS0VZLS0tLS0NCk1JSUJJakFOQmdrcWhraUc5dzBCQVFFRkFBT0NBUThBTUlJQkNnS0NBUUVBckMwdmx4RUtVUnN3NklSNzJGMTgNCm9oQWRhVmoxYUJIYmpRWk52a0tkODNLNElScHBkZGpEWFd2eTF2ZW01WmNJcTRreFhhdWFKdk10R3N4VGRhdFgNCklxbExlRFFucGZURGp0bXpKOUcvdFBBZVc1SnczK2drTS95YW40YUgrUmdmYXl1anYvMnQ3MThQQ0lyNm1SVlYNCjhxbW9XbU5NTWxONDlYUFdleTIreGQyUnozdStrM0Z0RlBUcEJWSWFYTncyZWlGMGtJRzh6UWlnQUt1b2RSdnMNCkVpVXJHcVV5NjNnRmFIVFRaZ2pWOXJKTXNyRWxVMWIxanIyMWkxTGZ1K3BVNnJSbjJqcWVtaTY2ZkExdnU5QTUNCmdmR0VOZ1FvNnp0a1NZZHBHL2dtdVZlUVl2V1JHcU9oZHRydnlMb2xwbzNYWFZpS285OXBBcVhJckVhcjRIeGYNCk9RSURBUUFCDQotLS0tLUVORCBQVUJMSUMgS0VZLS0tLS0='
            };
            $scope.emailExists = false;
            $scope.cantCreateUser = false;
            Register.create(data)
                .then(function (response) {
                    // Account Created
                    console.log(response);
                    UserLogin.loginEmail(g_email, 'LS0tLS1CRUdJTiBQVUJMSUMgS0VZLS0tLS0NCk1JSUJJakFOQmdrcWhraUc5dzBCQVFFRkFBT0NBUThBTUlJQkNnS0NBUUVBckMwdmx4RUtVUnN3NklSNzJGMTgNCm9oQWRhVmoxYUJIYmpRWk52a0tkODNLNElScHBkZGpEWFd2eTF2ZW01WmNJcTRreFhhdWFKdk10R3N4VGRhdFgNCklxbExlRFFucGZURGp0bXpKOUcvdFBBZVc1SnczK2drTS95YW40YUgrUmdmYXl1anYvMnQ3MThQQ0lyNm1SVlYNCjhxbW9XbU5NTWxONDlYUFdleTIreGQyUnozdStrM0Z0RlBUcEJWSWFYTncyZWlGMGtJRzh6UWlnQUt1b2RSdnMNCkVpVXJHcVV5NjNnRmFIVFRaZ2pWOXJKTXNyRWxVMWIxanIyMWkxTGZ1K3BVNnJSbjJqcWVtaTY2ZkExdnU5QTUNCmdmR0VOZ1FvNnp0a1NZZHBHL2dtdVZlUVl2V1JHcU9oZHRydnlMb2xwbzNYWFZpS285OXBBcVhJckVhcjRIeGYNCk9RSURBUUFCDQotLS0tLUVORCBQVUJMSUMgS0VZLS0tLS0=')
                        .then(function (response) {
                            $cookies.put('jwt', response.data.jwt);
                            $cookies.put('userId', response.data.userId);
                            $scope.isAuthenticated = true;
                            $scope.userId = response.data.userId;
                            userId = response.data.userId;

                            $http.get("http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/user/" + $scope.userId, {
                                transformRequest: angular.identity,
                                headers: {
                                    'Content-Type': undefined
                                },
                                id: $scope.userId
                            })
                                .success(function (response) {
                                    console.log(response);
                                    sb.connect($scope.userId, accessToken, function(user, error) {
                                        currentUser = user;
                                        if (error) {
                                            console.log(error);
                                        }

                                        $http.get("http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/user/" + $scope.userId, {
                                            transformRequest: angular.identity,
                                            headers: {
                                                'Content-Type': undefined
                                            },
                                            id: $scope.userId
                                        })
                                            .success(function (response) {
                                                myImageId = response.profileImageId;
                                                myName = response.name;
                                            })
                                            .error(function (response) {
                                                console.log(response);
                                            })
                                            .then(function () {
                                                $http.get("http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/image/" + myImageId, {
                                                    transformRequest: angular.identity,
                                                    headers: {
                                                        'Content-Type': undefined
                                                    },
                                                    imageId: myImageId
                                                })
                                                    .success(function (response) {
                                                        myImageLink = 'http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/image/file/' + response.filename;
                                                        sb.updateCurrentUserInfo(myName, myImageLink, function(response, error) {
                                                            console.log(response, error);
                                                        });
                                                    })
                                                    .error(function (response) {
                                                        console.log(response);
                                                    });
                                            });

                                    });
                                })
                                .error(function (response) {
                                    console.log(response);
                                })
                                .then(function () {
                                    $location.path("/home");
                                });
                        }, function (response) {
                            if (response.data.statusCode === 401) {
                                $scope.invalidEmailOrPass = true;
                            }
                            if (response.data.statusCode === 500) {
                                $scope.cantFindUser = true;
                            }
                            if (response.data.statusCode === 422) {
                                $scope.blankFields = true;
                            }
                        });
                }, function (response) {
                    if (response.data.statusCode === 409) {
                        UserLogin.loginEmail(g_email, 'LS0tLS1CRUdJTiBQVUJMSUMgS0VZLS0tLS0NCk1JSUJJakFOQmdrcWhraUc5dzBCQVFFRkFBT0NBUThBTUlJQkNnS0NBUUVBckMwdmx4RUtVUnN3NklSNzJGMTgNCm9oQWRhVmoxYUJIYmpRWk52a0tkODNLNElScHBkZGpEWFd2eTF2ZW01WmNJcTRreFhhdWFKdk10R3N4VGRhdFgNCklxbExlRFFucGZURGp0bXpKOUcvdFBBZVc1SnczK2drTS95YW40YUgrUmdmYXl1anYvMnQ3MThQQ0lyNm1SVlYNCjhxbW9XbU5NTWxONDlYUFdleTIreGQyUnozdStrM0Z0RlBUcEJWSWFYTncyZWlGMGtJRzh6UWlnQUt1b2RSdnMNCkVpVXJHcVV5NjNnRmFIVFRaZ2pWOXJKTXNyRWxVMWIxanIyMWkxTGZ1K3BVNnJSbjJqcWVtaTY2ZkExdnU5QTUNCmdmR0VOZ1FvNnp0a1NZZHBHL2dtdVZlUVl2V1JHcU9oZHRydnlMb2xwbzNYWFZpS285OXBBcVhJckVhcjRIeGYNCk9RSURBUUFCDQotLS0tLUVORCBQVUJMSUMgS0VZLS0tLS0=')
                            .then(function (response) {
                                $cookies.put('jwt', response.data.jwt);
                                $cookies.put('userId', response.data.userId);
                                $scope.isAuthenticated = true;
                                $scope.userId = response.data.userId;
                                userId = response.data.userId;

                                $http.get("http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/user/" + $scope.userId, {
                                    transformRequest: angular.identity,
                                    headers: {
                                        'Content-Type': undefined
                                    },
                                    id: $scope.userId
                                })
                                    .success(function (response) {
                                        console.log(response);
                                        sb.connect($scope.userId, accessToken, function(user, error) {
                                            currentUser = user;
                                            if (error) {
                                                console.log(error);
                                            }

                                            $http.get("http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/user/" + $scope.userId, {
                                                transformRequest: angular.identity,
                                                headers: {
                                                    'Content-Type': undefined
                                                },
                                                id: $scope.userId
                                            })
                                                .success(function (response) {
                                                    myImageId = response.profileImageId;
                                                    myName = response.name;
                                                })
                                                .error(function (response) {
                                                    console.log(response);
                                                })
                                                .then(function () {
                                                    $http.get("http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/image/" + myImageId, {
                                                        transformRequest: angular.identity,
                                                        headers: {
                                                            'Content-Type': undefined
                                                        },
                                                        imageId: myImageId
                                                    })
                                                        .success(function (response) {
                                                            myImageLink = 'http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/image/file/' + response.filename;
                                                            sb.updateCurrentUserInfo(myName, myImageLink, function(response, error) {
                                                                console.log(response, error);
                                                            });
                                                        })
                                                        .error(function (response) {
                                                            console.log(response);
                                                        });
                                                });

                                        });
                                    })
                                    .error(function (response) {
                                        console.log(response);
                                    })
                                    .then(function () {
                                        $location.path("/home");
                                    });
                            }, function (response) {
                                if (response.data.statusCode === 401) {
                                    $scope.invalidEmailOrPass = true;
                                }
                                if (response.data.statusCode === 500) {
                                    $scope.cantFindUser = true;
                                }
                                if (response.data.statusCode === 422) {
                                    $scope.blankFields = true;
                                }
                            });
                    }
                    if (response.data.statusCode === 500) {

                    }
                });



        }
    });
    $scope.$on('event:google-plus-signin-failure', function (event,authResult) {
        console.log(event, authResult);
        // Auth failure or signout detected
    });

    $scope.login = function (event) {
        $scope.invalidEmailOrPass = false;
        $scope.cantFindUser = false;
        $scope.blankFields = false;
        event.preventDefault();
        UserLogin.login($scope.username, $scope.password)
            .then(function (response) {
                $cookies.put('jwt', response.data.jwt);
                $cookies.put('userId', response.data.userId);
                $scope.isAuthenticated = true;
                $scope.userId = response.data.userId;
                userId = response.data.userId;

                $http.get("http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/user/" + $scope.userId, {
                    transformRequest: angular.identity,
                    headers: {
                        'Content-Type': undefined
                    },
                    id: $scope.userId
                })
                    .success(function (response) {
                        console.log(response);
                        sb.connect($scope.userId, accessToken, function(user, error) {
                            currentUser = user;
                            if (error) {
                                console.log(error);
                            }

                            $http.get("http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/user/" + $scope.userId, {
                                transformRequest: angular.identity,
                                headers: {
                                    'Content-Type': undefined
                                },
                                id: $scope.userId
                            })
                                .success(function (response) {
                                    myImageId = response.profileImageId;
                                    myName = response.name;
                                })
                                .error(function (response) {
                                    console.log(response);
                                })
                                .then(function () {
                                    $http.get("http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/image/" + myImageId, {
                                        transformRequest: angular.identity,
                                        headers: {
                                            'Content-Type': undefined
                                        },
                                        imageId: myImageId
                                    })
                                        .success(function (response) {
                                            myImageLink = 'http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/image/file/' + response.filename;
                                            sb.updateCurrentUserInfo(myName, myImageLink, function(response, error) {
                                                console.log(response, error);
                                            });
                                        })
                                        .error(function (response) {
                                            console.log(response);
                                        });
                                });

                        });
                    })
                    .error(function (response) {
                        console.log(response);
                    })
                    .then(function () {
                        $location.path("/home");
                    });
            }, function (response) {
                if (response.data.statusCode === 401) {
                    $scope.invalidEmailOrPass = true;
                }
                if (response.data.statusCode === 500) {
                    $scope.cantFindUser = true;
                }
                if (response.data.statusCode === 422) {
                    $scope.blankFields = true;
                }
            })
    };

    $scope.logout = function (event) {
        console.log(event);
        $cookies.remove('userId');
        $scope.username = '';
        $scope.password = '';
        $scope.response = '';
        $scope.userId = '';
        $scope.isAuthenticated = false;
        $cookies.remove('jwt');
        sb.disconnect(function () {});
        $location.path("/");
    };
});

// Factory For Create Quest Function
app.factory('QCreate', ['$http', function ($http) {
    return {
        create: function (name, rewardItemId, description, address, latitude, longitude, cashReward) {
            var data = {
                name: name,
                rewardItemId: rewardItemId,
                description: description,
                address: address,
                latitude: latitude,
                longitude: longitude,
                cashReward: cashReward

            };
            console.log(JSON.stringify(data));
            return $http({
                method: 'post',
                url: 'http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/quest',
                data: JSON.stringify(data),
                headers: {'Content-Type': 'application/json'}
            })
        },
        createRewardOnly: function(name, rewardItemId, description, address, latitude, longitude) {
            var data = {
                name: name,
                rewardItemId: rewardItemId,
                description: description,
                address: address,
                latitude: latitude,
                longitude: longitude

            };
            console.log(JSON.stringify(data));
            return $http({
                method: 'post',
                url: 'http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/quest',
                data: JSON.stringify(data),
                headers: {'Content-Type': 'application/json'}
            })
        },
        createCashOnly: function(name, description, address, latitude, longitude, cashReward) {
            var data = {
                name: name,
                description: description,
                address: address,
                latitude: latitude,
                longitude: longitude,
                cashReward: cashReward
            };
            console.log(JSON.stringify(data));
            return $http({
                method: 'post',
                url: 'http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/quest',
                data: JSON.stringify(data),
                headers: {'Content-Type': 'application/json'}
            })
        }
    }
}]);

// fileModel Directive For File Upload
app.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;

            element.bind('change', function () {
                scope.$apply(function () {
                    modelSetter(scope, element[0].files[0]);
                })
            })
        }
    }
}]);

// Factory For Quest Edit Page Function
app.factory('TagQuest', ['$http', function ($http) {
    return {
        tag: function (questId, tagId) {
            return $http({
                method: 'put',
                url: 'http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/quest/' + questId + '/tag/' + tagId,
                headers: {'Content-Type': 'application/json'},
                questId: questId,
                tagId: tagId
            })
        }
    }
}]);

// Controller For Quest Creation Page
app.controller('QuestCreateCtrl', function ($scope, $http, $location, NgMap, $uibModal, QCreate, TagQuest) {
    $scope.name = '';
    $scope.description = '';
    $scope.address = 'Orlando, FL';
    $scope.latitude = '';
    $scope.longitude = '';
    $scope.response = '';
    $scope.cashReward = 0.00;
    $scope.rewardItemId = '';
    $scope.createdItem = false;
    $scope.createdItemName = '';
    $scope.selectedItem = false;

    $scope.needRewardItem = false;

    $scope.nameBlank = false;

    $scope.tag = '';
    $scope.tags = [];

    $http.get("http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/tag", {
        transformRequest: angular.identity,
        headers: {
            'Content-Type': undefined
        }
    })
        .success(function (response) {
            $scope.tags = response;
            console.log(response);
        })
        .error(function (response) {
            console.log(response);
        });

    $scope.garb = '';

    NgMap.getMap().then(function (map) {
        console.log(map.getCenter());
        console.log('markers', map.markers);
        console.log('shapes', map.shapes);

    });

    $scope.createQuest = function (event) {
        $scope.invalidEmailOrPass = false;
        $scope.cantFindUser = false;
        $scope.blankFields = false;
        $scope.needRewardItem = false;
        $scope.nameBlank = false;

        if ($scope.name === '') {
            $scope.nameBlank = true;
            return;
        }

        // Google Maps API Key: AIzaSyAAhmUiENQie3Us3pxPWpSCZ_q8n2aSFJg
        var google_api_link = 'https://maps.googleapis.com/maps/api/geocode/json?address=' + $scope.address + '&key=AIzaSyAAhmUiENQie3Us3pxPWpSCZ_q8n2aSFJg';
        $http.get(google_api_link)
            .success(function (response) {
                console.log(response);
                $scope.latitude = response.results[0].geometry.location.lat;
                $scope.longitude = response.results[0].geometry.location.lng;
            })
            .error(function (response) {
            })
            .then(function () {

                if ($scope.name !== '' && ($scope.rewardItemId !== '' && $scope.rewardItemId !== null && $scope.cashReward > 0.00)) {
                    QCreate.create($scope.name, $scope.rewardItemId, $scope.description,
                        $scope.address, $scope.latitude, $scope.longitude, $scope.cashReward)
                        .success (function (response) {
                            console.log(response);
                            $scope.response = response;
                            var questId = response.questId;
                            TagQuest.tag(questId, $scope.tag.tagId)
                                .then(function (response) {
                                    console.log(response)
                                }, function (response) {
                                    console.log(response)
                                });
                            $location.path("/quests/created")
                    }).error (function (response) {
                        console.log(response);
                        if (response.statusCode === 422){
                            $scope.needRewardItem = true;
                        }
                        $scope.response = response;
                    });
                }
                else if($scope.name !== '' && ($scope.rewardItemId !== '' && $scope.cashReward <= 0.00)) {
                    QCreate.createRewardOnly($scope.name, $scope.rewardItemId, $scope.description,
                        $scope.address, $scope.latitude, $scope.longitude)
                        .success (function (response) {
                            console.log(response);
                            $scope.response = response;
                            var questId = response.questId;
                            TagQuest.tag(questId, $scope.tag.tagId)
                                .then(function (response) {
                                    console.log(response)
                                }, function (response) {
                                    console.log(response)
                                });
                            $location.path("/quests/created")
                        }).error (function (response) {
                        console.log(response);
                        if (response.statusCode === 422){
                            $scope.needRewardItem = true;
                        }
                        $scope.response = response;
                    });
                }
                else if($scope.name !== '' && ($scope.rewardItemId === '' && $scope.cashReward >= 0.00)) {
                    QCreate.createCashOnly($scope.name, $scope.description, $scope.address, $scope.latitude,
                        $scope.longitude, $scope.cashReward)
                        .success (function (response) {
                            console.log(response);
                            $scope.response = response;
                            var questId = response.questId;
                            TagQuest.tag(questId, $scope.tag.tagId)
                                .then(function (response) {
                                    console.log(response)
                                }, function (response) {
                                    console.log(response)
                                });
                            $location.path("/quests/created")
                        }).error (function (response) {
                        console.log(response);
                        if (response.statusCode === 422){
                            $scope.needRewardItem = true;
                        }
                        $scope.response = response;
                    });
                }
            });
    };

    $scope.animationsEnabled = true;
    $scope.open = function (size) {

        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: '/templates/modal_contents_item_create.html',
            controller: 'ModalInstanceCtrl',
            size: 'lg'
        });

        modalInstance.result.then(function (selectedItem) {
            $scope.rewardItemId = selectedItem;

            $http.get("http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/item", {
                transformRequest: angular.identity,
                headers: {
                    'Content-Type': undefined
                }
            })
                .success(function (response) {
                    console.log(response);
                    var allItems = response;
                    var arrLength = allItems.length;
                    for (var i = 0; i < arrLength; i++) {
                        if (allItems[i].itemId === $scope.rewardItemId) {
                            $scope.createdItem = true;
                            $scope.selectedItem = false;
                            $scope.createdItemName = allItems[i].name;
                        }
                    }
                })
                .error(function (response) {
                    console.log(response);
                });

        }, function () {
            console.log('Modal dismissed at: ' + new Date());
        });
    };
    $scope.toggleAnimation = function () {
        $scope.animationsEnabled = !$scope.animationsEnabled;
    };

    $scope.openInventory = function (size) {
        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: '/templates/modal_contents_item_inventory.html',
            controller: 'ModalInstanceCtrlInventory',
            size: size
        });

        modalInstance.result.then( function(selectedItem) {
            $scope.rewardItemId = selectedItem;

            $http.get("http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/item", {
                transformRequest: angular.identity,
                headers: {
                    'Content-Type': undefined
                }
            })
                .success(function (response) {
                    console.log(response);
                    var allItems = response;
                    var arrLength = allItems.length;
                    for (var i = 0; i < arrLength; i++) {
                        if (allItems[i].itemId === $scope.rewardItemId) {
                            $scope.createdItem = false;
                            $scope.selectedItem = true;
                            $scope.createdItemName = allItems[i].name;
                        }
                    }
                })
                .error(function (response) {
                    console.log(response);
                });

        }, function () {
            console.log('Modal dismissed at: ' + new Date());
        })
    };

    $scope.openCreateTagModal = function (size) {

        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: '/templates/modal_contents_tag_create.html',
            controller: 'ModalTagCreateCtrl',
            size: size
        });

        modalInstance.result.then(function (createdItem) {
            console.log(createdItem);
            $scope.tags = [];
            $http.get("http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/tag", {
                transformRequest: angular.identity,
                headers: {
                    'Content-Type': undefined
                }
            })
                .success(function (response) {
                    $scope.tags = response;
                    console.log(response);
                })
                .error(function (response) {
                    console.log(response);
                });

        }, function () {
            console.log('Modal dismissed at: ' + new Date());
        });
    };

});

// Controller For Tag Creation from Quest Creation Page
app.controller('ModalTagCreateCtrl', function ($scope, $uibModalInstance, $http, TagCreate) {
    $scope.tagName = '';

    $scope.ok = function () {
        TagCreate.create($scope.tagName)
            .then(function (response) {
                $uibModalInstance.close(response);
            }, function (response) {
                $scope.response = response;
            });
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});

// Factory For Tag Creation
app.factory('TagCreate', ['$http', function ($http) {
    return {
        create: function (name) {
            var data = {
                name: name
            };
            console.log(JSON.stringify(data));
            return $http({
                method: 'post',
                url: 'http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/tag',
                data: JSON.stringify(data),
                headers: {'Content-Type': 'application/json'}
            })
        }
    }
}]);

// Factory For Quest Edit Page Function
app.factory('UpdateQuest', ['$http', function ($http) {
    return {
        update: function (data, questId) {
            console.log(JSON.stringify(data));
            return $http({
                method: 'put',
                url: 'http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/quest/' + questId,
                data: JSON.stringify(data),
                headers: {'Content-Type': 'application/json'}
            })
        }
    }
}]);

// Controller For Quest Editing Page
app.controller('QuestEditCtrl', function ($scope, $http, $location, NgMap, $routeParams, $cookies, UpdateQuest) {
    $scope.userId = $cookies.get('userId');
    $scope.questId = $routeParams.questId;
    $scope.name = '';
    $scope.description = '';
    $scope.address = 'Orlando, FL';
    $scope.latitude = '';
    $scope.longitude = '';

    $scope.garb = '';

    $http.get("http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/quest/" + $scope.questId, {
        transformRequest: angular.identity,
        headers: {
            'Content-Type': undefined
        },
        questId: $scope.questId
    })
        .success(function (response) {
            console.log(response);
            $scope.name = response.name;
            $scope.description = response.description;
            $scope.address = response.address;
            $scope.latitude = response.latitude;
            $scope.longitude = response.longitude;
        })
        .error(function (response) {
            console.log(response);
        });

    NgMap.getMap().then(function (map) {
        console.log(map.getCenter());
        console.log('markers', map.markers);
        console.log('shapes', map.shapes);

    });

    $scope.updateQuest = function (event) {
        console.log(event);
        var google_api_link = 'https://maps.googleapis.com/maps/api/geocode/json?address=' + $scope.address + '&key=AIzaSyAAhmUiENQie3Us3pxPWpSCZ_q8n2aSFJg';
        $http.get(google_api_link)
            .success(function (response) {
                console.log(response);
                $scope.latitude = response.results[0].geometry.location.lat;
                $scope.longitude = response.results[0].geometry.location.lng;
            })
            .error(function (response) {
            })
            .then(function () {

                var data = {
                    description: $scope.description,
                    address: $scope.address,
                    latitude: $scope.latitude,
                    longitude: $scope.longitude
                };


                UpdateQuest.update(data, $scope.questId)
                    .success(function (response) {
                        console.log(response);
                        $location.path("/quests/" + $scope.questId);
                    })
                    .error(function (response) {
                        console.log(response);
                    });
            });
    };
});

// Controller For Inventory Select Modal on Quest Creation Page
app.controller('ModalInstanceCtrlInventory', function ($scope, $uibModalInstance, $http, $cookies) {
    $scope.inventory = [];
    $scope.chosenItemId = '';

    $http.get("http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/item", {
        transformRequest: angular.identity,
        headers: {
            'Content-Type': undefined
        }
    })
        .success(function (response) {
            console.log(response);
            var allItems = response;
            var arrLength = allItems.length;
            for (var i = 0; i < arrLength; i++) {
                if (allItems[i].ownerId === $cookies.get('userId')) {
                    $scope.inventory.push({
                        dateAdded: allItems[i].dateAdded,
                        name: allItems[i].name,
                        condition: allItems[i].condition,
                        itemId: allItems[i].itemId
                    });
                }
            }
        })
        .error(function (response) {
            console.log(response);
        });

    $scope.choose = function(selectedItemId) {
        $scope.chosenItemId = selectedItemId;
    };

    $scope.ok = function () {
        $uibModalInstance.close($scope.chosenItemId);
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});

// Controller For Item Creation from Quest Creation Page
app.controller('ItemCreateCtrl', function ($scope, $http, Upload, ItemCreate, $location) {
    $scope.rewardImageId = '';
    $scope.rewardDescription = '';
    $scope.rewardName = '';
    $scope.rewardCondition = '';
    $scope.conditions = ['New', 'Like New', 'Good', 'Fair', 'Poor'];
    $scope.rewardFile = '';

    $scope.myImage = '';
    $scope.myCroppedImage = '';

    var handleFileSelect = function(evt) {
        var file=evt.currentTarget.files[0];
        var reader = new FileReader();
        reader.onload = function (evt) {
            $scope.$apply(function($scope) {
                $scope.myImage = evt.target.result;
            });
        };
        reader.readAsDataURL(file);
    };
    angular.element(document.querySelector('#fileInput')).on('change', handleFileSelect);


    $scope.ok = function () {
        var binary = atob($scope.myCroppedImage.split(',')[1]);
        var mimeString = $scope.myCroppedImage.split(',')[0].split(':')[1].split(';')[0];
        var array = [];
        for(var i = 0; i < binary.length; i++) {
            array.push(binary.charCodeAt(i));
        }

        var theBlob = new Blob([new Uint8Array(array)], {type: mimeString});
        theBlob.lastModifiedDate = new Date();
        theBlob.name = "my-image.png";

        console.log(theBlob);
        Upload.upload({
            url: 'http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/image',
            data: {file: theBlob}
        }).then(function (resp) {
            console.log('Success ' + resp + ' uploaded. Response: ' + resp);
            console.log(resp);
            $scope.rewardImageId = resp.data.id;

            ItemCreate.create($scope.rewardName, $scope.rewardImageId, $scope.rewardDescription, $scope.rewardCondition)
                .then(function (response) {
                    $scope.rewardItemId = response.data.itemId;
                    $location.path('/inventory');
                }, function (response) {
                    $scope.response = response;
                });
        }, function (resp) {
            console.log('Error status: ' + resp.status);
        }, function (evt) {
            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
        });

    };
});

// Factory For Item Creation
app.factory('ItemCreate', ['$http', function ($http) {
    return {
        create: function (name, imageId, description, condition) {
            var data = {
                name: name,
                imageId: imageId,
                description: description,
                condition: condition
            };
            console.log(JSON.stringify(data));
            return $http({
                method: 'post',
                url: 'http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/item',
                data: JSON.stringify(data),
                headers: {'Content-Type': 'application/json'}
            })
        }
    }
}]);

// Controller For User's Created Quest Page
app.controller('CreatedQuestCtrl', function ($scope, $http, $cookies) {
    $scope.userInput = '';
    $scope.questIds = [];
    $scope.questIdsToTag = {};
    $scope.quests = [];
    $scope.tags = {};

    $scope.tagList = [];
    $scope.tagForFilter = {
        tagName: 'ALL',
        tagId: ''

    };

    $http.get("http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/tag", {
        transformRequest: angular.identity,
        headers: {
            'Content-Type': undefined
        }
    }).success( function (response) {
        $scope.tagList = response;
        $scope.tagList.push({
            tagName: 'ALL',
            tagId: ''
        });
        var numTags = response.length;
        for (var j = 0; j < numTags; j++) {
            $scope.tags[response[j].tagId] = response[j].tagName;
        }
    }).error( function (response) {
    }).then(function () {
        $http.get("http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/quest", {
            transformRequest: angular.identity,
            headers: {
                'Content-Type': undefined
            }
        })
            .success(function (response) {
                console.log(response);
                var allQuests = response;
                var arrLength = allQuests.length;
                for (var i = 0; i < arrLength; i++) {
                    if (allQuests[i].giverId === $cookies.get('userId')) {
                        $scope.questIds.push(allQuests[i].questId);
                        if (allQuests[i].tags.length > 0) {
                            $scope.questIdsToTag[allQuests[i].questId] = ' - Tag: ' + $scope.tags[allQuests[i].tags[0]];
                        } else {
                            $scope.questIdsToTag[allQuests[i].questId] = '';
                        }
                    }
                }
                console.log(response);
            })
            .error(function (response) {
                console.log(response);
            })
            .then(function () {
                var idLength = $scope.questIds.length;
                for (var i = 0; i < idLength; i++) {
                    $http.get("http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/quest/" + $scope.questIds[i], {
                        transformRequest: angular.identity,
                        headers: {
                            'Content-Type': undefined
                        }
                    })
                        .success(function (response) {

                            $scope.quests.push({
                                questId: response.questId,
                                created: response.created,
                                name: response.name,
                                cashReward: response.cashReward,
                                hasReward: response.rewardItemId !== null
                            });
                        })
                        .error(function (response) {
                            console.log(response);
                        });
                }
            });
    });
});

// Controller For User's Active Quest Page
app.controller('ActiveQuestCtrl', function ($scope, $http, $cookies) {
    $scope.userInput = '';
    $scope.questIds = [];
    $scope.questIdsToTag = {};
    $scope.quests = [];
    $scope.tags = {};

    $scope.tagList = [];
    $scope.tagForFilter = {
        tagName: 'ALL',
        tagId: ''

    };

    $http.get("http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/tag", {
        transformRequest: angular.identity,
        headers: {
            'Content-Type': undefined
        }
    }).success( function (response) {
        $scope.tagList = response;
        $scope.tagList.push({
            tagName: 'ALL',
            tagId: ''
        });
        var numTags = response.length;
        for (var j = 0; j < numTags; j++) {
            $scope.tags[response[j].tagId] = response[j].tagName;
        }
    }).error( function (response) {
    }).then(function () {
        $http.get("http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/quest", {
            transformRequest: angular.identity,
            headers: {
                'Content-Type': undefined
            }
        })
            .success(function (response) {
                console.log(response);
                var allQuests = response;
                var arrLength = allQuests.length;
                for (var i = 0; i < arrLength; i++) {
                    if (allQuests[i].receiverId === $cookies.get('userId')) {
                        $scope.questIds.push(allQuests[i].questId);
                        if (allQuests[i].tags.length > 0) {
                            $scope.questIdsToTag[allQuests[i].questId] = ' - Tag: ' + $scope.tags[allQuests[i].tags[0]];
                        } else {
                            $scope.questIdsToTag[allQuests[i].questId] = '';
                        }
                    }
                }
                console.log(response);
            })
            .error(function (response) {
                console.log(response);
            })
            .then(function () {
                var idLength = $scope.questIds.length;
                for (var i = 0; i < idLength; i++) {
                    $http.get("http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/quest/" + $scope.questIds[i], {
                        transformRequest: angular.identity,
                        headers: {
                            'Content-Type': undefined
                        }
                    })
                        .success(function (response) {

                            $scope.quests.push({
                                questId: response.questId,
                                created: response.created,
                                name: response.name,
                                cashReward: response.cashReward,
                                hasReward: response.rewardItemId !== null
                            });
                        })
                        .error(function (response) {
                            console.log(response);
                        });
                }
            });
    });
});

// Controller For Quest Search Page
app.controller('QuestSearchCtrl', function ($scope, $http, $cookies) {
    $scope.userInput = '';
    $scope.questIds = [];
    $scope.questIdsToTag = {};
    $scope.quests = [];
    $scope.tags = {};

    $scope.tagList = [];
    $scope.tagForFilter = {
        tagName: 'ALL',
        tagId: ''
    };

    $http.get("http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/tag", {
        transformRequest: angular.identity,
        headers: {
            'Content-Type': undefined
        }
    }).success( function (response) {
        $scope.tagList = response;
        $scope.tagList.push({
            tagName: 'ALL',
            tagId: ''
        });
        var numTags = response.length;
        for (var j = 0; j < numTags; j++) {
            $scope.tags[response[j].tagId] = response[j].tagName;
        }
    }).error( function (response) {
    }).then(function () {
        $http.get("http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/quest", {
            transformRequest: angular.identity,
            headers: {
                'Content-Type': undefined
            }
        })
            .success(function (response) {
                console.log(response);
                var allQuests = response;
                var arrLength = allQuests.length;
                for (var i = 0; i < arrLength; i++) {
                    if (allQuests[i].giverId !== $cookies.get('userId') && allQuests[i].receiverId === null) {
                        $scope.questIds.push(allQuests[i].questId);
                        if (allQuests[i].tags.length > 0) {
                            $scope.questIdsToTag[allQuests[i].questId] = ' - Tag: ' + $scope.tags[allQuests[i].tags[0]];
                        } else {
                            $scope.questIdsToTag[allQuests[i].questId] = '';
                        }
                    }
                }
                console.log(response);
            })
            .error(function (response) {
                console.log(response);
            })
            .then(function () {
                var idLength = $scope.questIds.length;
                for (var i = 0; i < idLength; i++) {
                    $http.get("http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/quest/" + $scope.questIds[i], {
                        transformRequest: angular.identity,
                        headers: {
                            'Content-Type': undefined
                        }
                    })
                        .success(function (response) {
                            console.log(response);

                            $scope.quests.push({
                                questId: response.questId,
                                created: response.created,
                                name: response.name,
                                cashReward: response.cashReward,
                                hasReward: response.rewardItemId !== null
                            });
                        })
                        .error(function (response) {
                            console.log(response);
                        });
                }
            });
    });
});

// Factory For Inventory Upload Function
app.factory('InventoryUpload', ['$http', function ($http) {
    return {
        create: function (data) {
            console.log(JSON.stringify(data));
            return $http({
                method: 'post',
                url: 'http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/item',
                data: JSON.stringify(data),
                headers: {'Content-Type': 'application/json'}
            })
        }
    }
}]);

// Controller For Item Inventory Page
app.controller('InventoryCtrl', function ($scope, $http, $cookies, angularGridInstance, InventoryUpload, $uibModal, $route) {
    $scope.pics = [];
    $scope.picFilepaths = [];
    $scope.inventoryFile = '';
    $scope.newInventoryItemId = '';
    $scope.name = '';

    $scope.names = {};


    $http.get("http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/item", {
        transformRequest: angular.identity,
        headers: {
            'Content-Type': undefined
        }
    })
        .success(function (response) {
            console.log(response);
            var allItems = response;
            var arrLength = allItems.length;
            for (var i = 0; i < arrLength; i++) {

                (function () {
                if (allItems[i].ownerId === $cookies.get('userId')) {
                    var filename = '';
                    const infoCurr = allItems[i];
                    $http.get("http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/image/" + allItems[i].imageId, {
                        transformRequest: angular.identity,
                        headers: {
                            'Content-Type': undefined
                        },
                        imageId: allItems[i].imageId
                    })
                        .success(function (response) {
                            filename = 'http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/image/file/' + response.filename;
                        })
                        .error(function (response) {
                            console.log(response);
                        }).then(function() {
                            $scope.pics.push({
                                filename: filename,
                                id: infoCurr.imageId,
                                name: infoCurr.name,
                                description: infoCurr.description,
                                created: infoCurr.dateAdded,
                                condition: infoCurr.condition,
                                itemId: infoCurr.itemId
                            });
                    });
                }

                })();
            }
        })
        .error(function (response) {
            console.log(response);
        })
        .then(function () {

        });

    $scope.open = function (size) {
        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: '/templates/modal_contents_item_create_inventory.html',
            controller: 'ModalInstanceCtrl',
            size: size
        });

        modalInstance.result.then(function (selectedItem) {
            console.log(selectedItem);
            $route.reload();
        }, function () {
            console.log('Modal dismissed at: ' + new Date());
        });
    };

    $scope.deleteItem = function(event, item) {
        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: '/templates/modal_contents_confirmation.html',
            controller: 'ModalConfirmationCtrl',
            size: 'sm',
            resolve: {
                presentDialog: function () {
                    return {
                        header: 'Delete This Item?',
                        bodyText: 'Are you sure you want to delete this item?',
                        buttonText: 'Delete',
                        buttonStyle: 'btn-danger'
                    };
                }
            }
        });

        modalInstance.result.then(function () {
            $http.delete("http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/item/" + item.itemId, {
                transformRequest: angular.identity,
                headers: {
                    'Content-Type': undefined
                },
                id: item.itemId
            })
                .success(function (response) {
                    console.log(response);
                    $route.reload();
                })
                .error(function (response) {
                    console.log(response);
                });
        }, function () {
            console.log('Modal dismissed at: ' + new Date());
        });
    };

    $scope.editItem = function(event, item) {
        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: '/templates/modal_contents_item_edit.html',
            controller: 'ModalItemEditCtrl',
            size: 'md',
            resolve: {
                itemToEdit: function() {
                    return {
                        item: item
                    };
                }
            }
        });

        modalInstance.result.then(function () {
            $route.reload();
        }, function () {
            console.log('Modal dismissed at: ' + new Date());
        });
    }
});

// Factory For Item Edit Function
app.factory('UpdateItem', ['$http', function ($http) {
    return {
        update: function (data) {
            console.log(JSON.stringify(data));
            return $http({
                method: 'put',
                url: 'http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/item',
                data: JSON.stringify(data),
                headers: {'Content-Type': 'application/json'}
            })
        }
    }
}]);

// Controller For Edit Item
app.controller('ModalItemEditCtrl', function ($scope, $uibModalInstance, $cookies, UpdateItem, itemToEdit) {
    console.log(itemToEdit.item);
    $scope.itemId = itemToEdit.item.itemId;
    $scope.name = itemToEdit.item.name;
    $scope.description = itemToEdit.item.description;
    $scope.condition = itemToEdit.item.condition;
    $scope.conditions = ['New', 'Like New', 'Good', 'Fair', 'Poor'];

    $scope.ok = function () {
        var data = {
            itemId: $scope.itemId
        };

        if ($scope.name !== '' && $scope.name !== itemToEdit.item.name) {
            data.name = $scope.name;
        }
        if ($scope.description !== '' && $scope.description !== itemToEdit.item.description) {
            data.description = $scope.description;
        }
        if($scope.condition !== '' && $scope.condition !== itemToEdit.item.condition) {
            data.condition = $scope.condition;
        }

        UpdateItem.update(data)
            .then(function (response) {
                console.log(response);
                $uibModalInstance.close();
            }, function (response) {
                console.log(response);
            });
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});

// Controller For Quest Page
app.controller('QuestCtrl', function ($scope, $http, $cookies, $routeParams, $uibModal, $location) {
    $scope.userId = $cookies.get('userId');
    $scope.questId = $routeParams.questId;
    $scope.questName = '';
    $scope.rewardImageId = '';
    $scope.questExp = '';
    $scope.rewardDescription = '';
    $scope.rewardName = '';
    $scope.rewardCondition = '';
    $scope.rewardItemId = '';
    $scope.rewardDateAdded = '';
    $scope.questDescription = '';
    $scope.address = '';
    $scope.latitude = '';
    $scope.longitude = '';
    $scope.created = '';
    $scope.completed = '';
    $scope.receiverId = '';
    $scope.giverId = '';
    $scope.status = '';
    $scope.giverRating = '';
    $scope.receiverRating = '';
    $scope.cashReward = '';
    $scope.receiverComment = '';
    $scope.giverComment = '';
    $scope.applicants = '';
    $scope.rewardImagePath = '';
    $scope.giverName = '';
    $scope.giverImagePath = '';
    $scope.giverImageId = '';

    $scope.questOpen = false;
    $scope.questInProgress = false;
    $scope.questWaitingOnGiver = false;
    $scope.questCompleted = false;

    $scope.applied = false;
    $scope.questDoer = {};
    $scope.questDoerBool = false;
    $scope.questGiver = false;

    $scope.applicantData = [];

    $scope.questDoerData = {};


    $scope.acceptResp = '';

    $scope.mess = '';

    $scope.tag = '';
    $scope.tagId = '';
    $scope.tags = [];

    $scope.reportCreated = false;

    $http.get("http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/tag", {
        transformRequest: angular.identity,
        headers: {
            'Content-Type': undefined
        }
    })
        .success(function (response) {
            $scope.tags = response;
        })
        .error(function (response) {

        });

    $http.get("http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/quest", {
        transformRequest: angular.identity,
        headers: {
            'Content-Type': undefined
        }
    }).success(function (response) {
        var numQuests = response.length;
        for (var questCount = 0; questCount < numQuests; questCount++) {
            if (response[questCount].questId !== $scope.questId){
                continue;
            }
            $scope.tagId = response[questCount].tags[0];
            var numTags = $scope.tags.length;
            for (var tagCount = 0; tagCount < numTags; tagCount++) {
                if ($scope.tagId === $scope.tags[tagCount].tagId) {
                    $scope.tag = $scope.tags[tagCount].tagName;
                }
            }
        }
    }).error(function (response) {

    });

    $http.get("http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/quest/" + $scope.questId, {
        transformRequest: angular.identity,
        headers: {
            'Content-Type': undefined
        },
        questId: $scope.questId
    })
        .success(function (response) {
            console.log(response);
            $scope.questName = response.name;
            $scope.rewardItemId = response.rewardItemId;
            $scope.questDescription = response.description;
            $scope.address = response.address;
            $scope.latitude = response.latitude;
            $scope.longitude = response.longitude;
            $scope.created = response.created;
            $scope.completed = response.completed;
            $scope.receiverId = response.receiverId;
            $scope.giverId = response.giverId;
            $scope.status = response.status;
            $scope.giverRating = response.giverRating;
            $scope.receiverRating = response.receiverRating;
            $scope.cashReward = response.cashReward;
            $scope.receiverComment = response.receiverComment;
            $scope.giverComment = response.giverComment;
            $scope.applicants = response.applicants;
            $scope.questExp = response.rewardExp;
        })
        .error(function (response) {
            console.log(response);
        })
        .then(function () {
            $http.get("http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/item", {
                transformRequest: angular.identity,
                headers: {
                    'Content-Type': undefined
                }
            })
                .success(function (response) {
                    var allItems = response;
                    var arrLength = allItems.length;
                    for (var k = 0; k < arrLength; k++) {
                        if (allItems[k].itemId === $scope.rewardItemId) {
                            $http.get("http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/image/" + allItems[k].imageId, {
                                transformRequest: angular.identity,
                                headers: {
                                    'Content-Type': undefined
                                },
                                imageId: allItems[k].imageId
                            })
                                .success(function (response) {
                                    console.log(response);
                                    $scope.rewardImagePath = 'http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/image/file/' + response.filename;
                                })
                                .error(function (response) {
                                    console.log(response);
                                });

                            $scope.rewardCondition = allItems[k].condition;
                            $scope.rewardName = allItems[k].name;
                            $scope.rewardDescription = allItems[k].description;
                        }
                    }
                })
                .error(function (response) {
                    console.log(response);
                });

            // Get Quest Creator Details
            $http.get("http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/user/" + $scope.giverId, {
                transformRequest: angular.identity,
                headers: {
                    'Content-Type': undefined
                },
                id: $scope.giverId
            })
                .success(function (response) {
                    console.log(response);
                    $scope.giverName = response.name;
                    $scope.giverImageId = response.profileImageId;
                })
                .error(function (response) {
                    console.log(response);
                })
                .then(function () {
                    // Get Quest Giver's Profile Image File Path
                    $http.get("http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/image/" + $scope.giverImageId, {
                        transformRequest: angular.identity,
                        headers: {
                            'Content-Type': undefined
                        },
                        imageId: $scope.giverImageId
                    })
                        .success(function (response) {
                            console.log(response);
                            $scope.giverImagePath = 'http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/image/file/' + response.filename;
                        })
                        .error(function (response) {
                            console.log(response);
                        });
                });

            // Update status
            $scope.questOpen = false;
            $scope.questInProgress = false;
            $scope.questWaitingOnGiver = false;
            $scope.questCompleted = false;
            $scope.applied = false;
            if($scope.status === 'OPEN'){
                $scope.questOpen = true;
            }
            else if($scope.status === 'IN_PROGRESS'){
                $scope.questInProgress = true;
            }
            else if($scope.status === 'WAITING_ON_QUEST_GIVER'){
                $scope.questWaitingOnGiver = true;
            }
            else if($scope.status === 'COMPLETED'){
                $scope.questCompleted = true;
            }

            if ($scope.giverId === $cookies.get('userId')) {
                $scope.questGiver = true;
            }
            if ($scope.receiverId === $cookies.get('userId')) {
                $scope.questDoerBool = true;
            }
            if ($scope.applicants.indexOf($cookies.get('userId')) !== -1) {
                $scope.applied = true;
            }

            // Get Applicant Data
            if ($scope.applicants.length !== 0 && $scope.giverId === $cookies.get('userId')){
                var allApps = $scope.applicants;
                var arrLength = allApps.length;
                for (var i = 0; i < arrLength; i++) {
                    $http.get("http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/user/" + allApps[i], {
                        transformRequest: angular.identity,
                        headers: {
                            'Content-Type': undefined
                        },
                        id: allApps[i]
                    })
                        .success(function (response) {
                            var imageLink;
                            console.log(response);
                            $http.get("http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/image/" + response.profileImageId, {
                                transformRequest: angular.identity,
                                headers: {
                                    'Content-Type': undefined
                                },
                                imageId: response.profileImageId
                            })
                                .success(function (response) {
                                    imageLink = 'http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/image/file/' + response.filename;
                                })
                                .error(function (response) {
                                })
                                .then( function () {
                                    $scope.applicantData.push({
                                        name: response.name,
                                        id: response.userId,
                                        level: response.level,
                                        profileImageId: response.profileImageId,
                                        email: response.email,
                                        profileImageLink: imageLink
                                    });
                            });
                        })
                        .error(function (response) {
                            console.log(response);
                        }).then( function () {

                    });
                }
            }

            // Get Quest Doer Data
            if($scope.questInProgress) {
                $http.get("http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/user/" + $scope.receiverId, {
                    transformRequest: angular.identity,
                    headers: {
                        'Content-Type': undefined
                    },
                    id: $scope.receiverId
                })
                    .success(function (response) {
                        var imageLink;
                        console.log(response);
                        $http.get("http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/image/" + response.profileImageId, {
                            transformRequest: angular.identity,
                            headers: {
                                'Content-Type': undefined
                            },
                            imageId: response.profileImageId
                        })
                            .success(function (response) {
                                imageLink = 'http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/image/file/' + response.filename;
                            })
                            .error(function (response) {
                            })
                            .then( function () {
                                $scope.questDoer = {
                                    name: response.name,
                                    id: response.userId,
                                    level: response.level,
                                    profileImageId: response.profileImageId,
                                    email: response.email,
                                    profileImageLink: imageLink
                                };
                            });
                    })
                    .error(function (response) {
                        console.log(response);
                    }).then( function () {

                });
            }
        });

    $scope.animationsEnabled = true;

    $scope.apply = function (event) {
        console.log(event);
        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: '/templates/modal_contents_confirmation.html',
            controller: 'ModalConfirmationCtrl',
            size: 'sm',
            resolve: {
                presentDialog: function () {
                    return {
                        header: 'Apply For This Quest?',
                        bodyText: 'Are you sure you want to apply for this quest?',
                        buttonText: 'Apply',
                        buttonStyle: 'btn-success'
                    };
                }
            }
        });

        modalInstance.result.then(function () {
            $http.put("http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/quest/" + $scope.questId + '/apply/' +
                $cookies.get('userId'), {
                transformRequest: angular.identity,
                headers: {
                    'Content-Type': undefined
                },
                questId: $scope.questId,
                userId: $cookies.get('userId')
            })
                .success(function (response) {
                    console.log(response);
                    $scope.applied = true;
                })
                .error(function (response) {
                    console.log(response);
                });
        }, function () {
            console.log('Modal dismissed at: ' + new Date());
        });
    };

    $scope.completeQuest = function (event) {
        console.log(event);
        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: '/templates/modal_contents_confirmation.html',
            controller: 'ModalConfirmationCtrl',
            size: 'sm',
            resolve: {
                presentDialog: function () {
                    return {
                        header: 'Complete This Quest?',
                        bodyText: 'Are you sure you want to mark this quest as complete?',
                        buttonText: 'Complete',
                        buttonStyle: 'btn-success'
                    };
                }
            }
        });

        modalInstance.result.then(function () {
            $http.put("http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/quest/" + $scope.questId + '/complete', {
                transformRequest: angular.identity,
                headers: {
                    'Content-Type': undefined
                },
                questId: $scope.questId
            })
                .success(function (response) {
                    console.log(response);
                    if ($scope.giverId === $cookies.get('userId')){
                        $scope.questInProgress = false;
                        $scope.questWaitingOnGiver = false;
                        $scope.questCompleted = true;
                    }
                    if ($scope.receiverId === $cookies.get('userId')){
                        $scope.questInProgress = false;
                        $scope.questWaitingOnGiver = true;
                    }
                })
                .error(function (response) {
                    console.log(response);
                });
        }, function () {
            console.log('Modal dismissed at: ' + new Date());
        });
    };

    $scope.acceptApp = function(event, applicant) {
        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: '/templates/modal_contents_confirmation.html',
            controller: 'ModalConfirmationCtrl',
            size: 'sm',
            resolve: {
                presentDialog: function () {
                    return {
                        header: 'Accept This Quester?',
                        bodyText: 'Are you sure you want to accept this quester to complete your quest?',
                        buttonText: 'Accept',
                        buttonStyle: 'btn-success'
                    };
                }
            }
        });

        modalInstance.result.then(function () {
            $http.put("http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/quest/" + $scope.questId + "/accept/"
                      + applicant.id, {
                transformRequest: angular.identity,
                headers: {
                    'Content-Type': undefined
                },
                questId: $scope.questId,
                userId: $scope.userId
            })
                .success(function (response) {
                    console.log(response);
                    $scope.questOpen = false;
                    $scope.questInProgress = true;
                    $scope.questDoer = applicant;
                })
                .error(function (response) {
                    console.log(response);
                });
        }, function () {
            console.log('Modal dismissed at: ' + new Date());
        });
    };

    $scope.deleteQuest = function(event) {
        console.log(event);
        if ($scope.giverId !== $cookies.get('userId') || $scope.questOpen === false) {
            return;
        }
        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: '/templates/modal_contents_confirmation.html',
            controller: 'ModalConfirmationCtrl',
            size: 'sm',
            resolve: {
                presentDialog: function () {
                    return {
                        header: 'Delete This Quest?',
                        bodyText: 'Are you sure you want to delete this quest?',
                        buttonText: 'Delete',
                        buttonStyle: 'btn-danger'
                    };
                }
            }
        });

        modalInstance.result.then(function () {
            $http.delete("http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/quest/" + $scope.questId, {
                transformRequest: angular.identity,
                headers: {
                    'Content-Type': undefined
                },
                questId: $scope.questId
            })
                .success(function (response) {
                    console.log(response);
                    $location.path("/home")
                })
                .error(function (response) {
                    console.log(response);
                });
        }, function () {
            console.log('Modal dismissed at: ' + new Date());
        });
    };

    $scope.rateQuest = function(event) {
        console.log(event);

        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: '/templates/modal_contents_quest_rate.html',
            controller: 'ModalQuestRateCtrl',
            size: 'sm',
            resolve: {
                rateInfo: function () {
                    return {
                        questId: $scope.questId,
                        questName: $scope.questName,
                        questGiver: $scope.questGiver
                    };
                }
            }
        });

        modalInstance.result.then(function () {

        }, function () {
            console.log('Modal dismissed at: ' + new Date());
        });
    };

    $scope.reportQuest = function(event) {
        console.log(event);

        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: '/templates/modal_contents_quest_report.html',
            controller: 'ModalQuestReportCtrl',
            size: 'sm',
            resolve: {
                reportInfo: function () {
                    return {
                        questId: $scope.questId,
                        questName: $scope.questName,
                        giverId: $scope.giverId,
                        reporterId: $scope.userId
                    };
                }
            }
        });

        modalInstance.result.then(function () {
            $scope.reportCreated = true;
        }, function () {
            console.log('Modal dismissed at: ' + new Date());
        });
    };

    $scope.startChatWithApp = function(event, applicant) {
        $scope.mess = sb;
        sb.GroupChannel.createChannelWithUserIds([applicant.id], true, 'test', '', '', '', function(createdChannel, error){
            if (error) {
                console.log(applicant.id);
                console.error(error);
            }
            $location.path('/chat')
        });

    };
});

// Factory For Quest Report Function
app.factory('ReportQuest', ['$http', function ($http) {
    return {
        report: function (data) {
            console.log(JSON.stringify(data));
            return $http({
                method: 'post',
                url: 'http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/report',
                data: JSON.stringify(data),
                headers: {'Content-Type': 'application/json'}
            })
        }
    }
}]);

// Controller For Quest Report Modal
app.controller('ModalQuestReportCtrl', function ($scope, $uibModalInstance, reportInfo, ReportQuest) {
    $scope.questId = reportInfo.questId;
    $scope.questName = reportInfo.questName;
    $scope.giverId = reportInfo.giverId;
    $scope.reporterId = reportInfo.reporterId;
    $scope.selectedReason = '';
    $scope.reportReasons = ['Bad Offline Behavior', 'Inappropriate Messages', 'Inappropriate Photos',
        'Inappropriate Quest Details', 'Other', 'Spam Or Misleading'];
    $scope.reportMessage = '';
    $scope.errorMakingReport = false;

    $scope.ok = function () {
        $scope.errorMakingReport = false;
        var data = {
            reporterId: $scope.reporterId,
            offenderId: $scope.giverId,
            reason: $scope.selectedReason,
            message: $scope.reportMessage,
            questId: $scope.questId
        };
        ReportQuest.report(data)
            .success(function(response) {
                $uibModalInstance.close();
            })
            .error(function(response) {
                $scope.errorMakingReport = true;
                console.log(response);
            });
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});

// Factory For Quest Rating Function
app.factory('RateQuest', ['$http', function ($http) {
    return {
        rate: function (data, questId) {
            console.log(JSON.stringify(data));
            return $http({
                method: 'put',
                url: 'http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/quest/' + questId + '/rate',
                data: JSON.stringify(data),
                headers: {'Content-Type': 'application/json'}
            })
        }
    }
}]);

// Controller For Quest Rating Modal
app.controller('ModalQuestRateCtrl', function ($scope, $uibModalInstance, rateInfo, RateQuest) {
    $scope.questId = rateInfo.questId;
    $scope.questName = rateInfo.questName;
    $scope.questGiver = rateInfo.questGiver;
    $scope.questRating = 0;
    $scope.maxRating = 5;
    $scope.ratings = [1, 2, 3, 4, 5];

    $scope.range = function(n) {
        return new Array(n);
    };

    $scope.ok = function () {
        var data = {
            rate: $scope.questRating
        };
        RateQuest.rate(data, $scope.questId)
            .success(function(response) {
                console.log(response);
            });
        $uibModalInstance.close();
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.updateRating = function (passed) {
        $scope.questRating = passed;
        console.log(passed);
    }
});

// Controller For Confirmation
app.controller('ModalConfirmationCtrl', function ($scope, $uibModalInstance, presentDialog) {
    $scope.header = presentDialog.header;
    $scope.bodyText = presentDialog.bodyText;
    $scope.buttonText = presentDialog.buttonText;
    $scope.buttonStyle = presentDialog.buttonStyle;

    $scope.ok = function () {
        $uibModalInstance.close();
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});

// Controller For Profile Page
app.controller('ProfileCtrl', function ($scope, $http, $cookies, $routeParams, $uibModal) {
    $scope.userId = $routeParams.userId;
    $scope.name = '';
    $scope.level = '';
    $scope.exp = '';
    $scope.expToNextLevel = '';
    $scope.lastLogin = '';
    $scope.joined = '';
    $scope.cityId = '';
    $scope.profileImageId = '';
    $scope.profileImageLink = '';
    $scope.cityName = '';
    $scope.health = 0;
    $scope.maxHealth = 12;
    $scope.healthPerc = 100;
    $scope.healthType = 'success';
    $scope.ownProfile = $scope.userId === $cookies.get('userId');
    $scope.numRatings = 0;
    $scope.sumRatings = 0;
    $scope.avgRating = 5;
    $scope.needHalfStar = false;
    $scope.reportCreated = false;

    $scope.questIds2 = [];
    $scope.maxRating = 5;
    $scope.range = function(n) {
        if(n < 0) {
            n = 0;
        }
        return new Array(n);
    };

    $scope.questIds = [];
    $scope.questIdsToTag = {};
    $scope.quests = [];
    $scope.tags = {};

    $http.get("http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/tag", {
        transformRequest: angular.identity,
        headers: {
            'Content-Type': undefined
        }
    }).success( function (response) {
        var numTags = response.length;
        for (var j = 0; j < numTags; j++) {
            $scope.tags[response[j].tagId] = response[j].tagName;
        }
    }).error( function (response) {
    }).then(function () {
        $http.get("http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/quest/user/" + $scope.userId, {
            transformRequest: angular.identity,
            headers: {
                'Content-Type': undefined
            }
        })
            .success(function (response) {
                console.log(response);
                var allQuests = response;
                var arrLength = allQuests.length;
                for (var i = 0; i < arrLength; i++) {
                    if (allQuests[i].giverId === $scope.userId) {
                        $scope.questIds.push(allQuests[i].questId);
                        if (allQuests[i].tags.length > 0) {
                            $scope.questIdsToTag[allQuests[i].questId] =$scope.tags[allQuests[i].tags[0]];
                        } else {
                            $scope.questIdsToTag[allQuests[i].questId] = '';
                        }
                    }
                    if (allQuests[i].receiverId === $scope.userId) {
                        $scope.questIds2.push(allQuests[i].questId);
                    }
                }
                console.log(response);
            })
            .error(function (response) {
                console.log(response);
            })
            .then(function () {
                var idLength = $scope.questIds.length;
                for (var i = 0; i < idLength; i++) {
                    $http.get("http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/quest/" + $scope.questIds[i], {
                        transformRequest: angular.identity,
                        headers: {
                            'Content-Type': undefined
                        }
                    })
                        .success(function (response) {
                            if (response.giverRating !== null && response.giverRating !== 0){
                                $scope.numRatings += 1;
                                $scope.sumRatings += response.giverRating;
                                $scope.avgRating = Math.floor($scope.sumRatings / $scope.numRatings);
                                $scope.needHalfStar = ($scope.sumRatings / $scope.numRatings) - (Math.floor($scope.sumRatings / $scope.numRatings)) > .25;
                            }

                            $scope.quests.push({
                                questId: response.questId,
                                created: response.created,
                                name: response.name
                            });
                        })
                        .error(function (response) {
                            console.log(response);
                        });
                }

                idLength = $scope.questIds2.length;
                for (i=0; i < idLength; i++) {
                    $http.get('http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/quest/' + $scope.questIds2[i], {
                        transformRequest: angular.identity,
                        headers: {
                            'Content-Type': undefined
                        }
                    })
                        .success(function (response) {
                            if (response.receiverRating !== null && response.receiverRating !== 0) {
                                console.log('here');
                                $scope.numRatings += 1;
                                $scope.sumRatings += response.receiverRating;
                                $scope.avgRating = Math.floor($scope.sumRatings / $scope.numRatings);
                                $scope.needHalfStar = ($scope.sumRatings / $scope.numRatings) - (Math.floor($scope.sumRatings / $scope.numRatings)) > .25;
                            }
                        })
                        .error(function (response) {
                            console.log(response);
                        });
                }

            })
            .then(function () {
                if ($scope.numRatings > 0) {
                    $scope.avgRating = $scope.sumRatings / $scope.numRatings;
                }
                console.log($scope.numRatings);
            });
    });

    $http.get("http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/user/" + $scope.userId, {
        transformRequest: angular.identity,
        headers: {
            'Content-Type': undefined
        },
        id: $scope.userId
    })
        .success(function (response) {
            $scope.name = response.name;
            $scope.level = response.level;
            $scope.exp = response.exp;
            $scope.expToNextLevel = response.expToNextLevel;
            $scope.lastLogin = response.lastLogin;
            $scope.joined = response.joined;
            $scope.cityId = response.cityId;
            $scope.profileImageId = response.profileImageId;
            $scope.health = 12 - response.health;
        })
        .error(function (response) {
            console.log(response);
        })
        .then(function () {

            if ($scope.health > 6) {
                $scope.healthType = 'success';
            } else if ($scope.health > 0) {
                $scope.healthType = 'warning';
            } else {
                $scope.healthType = 'danger';
            }
            $scope.healthPerc = 100 * $scope.health / $scope.maxHealth;
            if ($scope.health === 0) {
                $scope.health = 0.25;
            }
            $http.get("http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/image/" + $scope.profileImageId, {
                transformRequest: angular.identity,
                headers: {
                    'Content-Type': undefined
                },
                imageId: $scope.profileImageId
            })
                .success(function (response) {
                    $scope.profileImageLink = 'http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/image/file/' + response.filename;
                })
                .error(function (response) {

                })
                .then(function () {
                    $http.get("http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/city", {
                        transformRequest: angular.identity,
                        headers: {
                            'Content-Type': undefined
                        }
                    })
                        .success(function (response) {
                            var cities = response;
                            var numCities = cities.length;
                            for (var i = 0; i < numCities; i++) {
                                if (cities[i].id === $scope.cityId) {
                                    $scope.cityName = cities[i].name
                                }
                            }
                        })
                        .error(function (response) {

                        });
            });
        });

    $scope.reportUser = function(event) {
        console.log(event);

        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: '/templates/modal_contents_user_report.html',
            controller: 'ModalUserReportCtrl',
            size: 'sm',
            resolve: {
                reportInfo: function () {
                    return {
                        offenderName: $scope.name,
                        offenderId: $scope.userId,
                        reporterId: $cookies.get('userId')
                    };
                }
            }
        });

        modalInstance.result.then(function () {
            $scope.reportCreated = true;
        }, function () {
            console.log('Modal dismissed at: ' + new Date());
        });
    };
});

// Controller For User Report Modal
app.controller('ModalUserReportCtrl', function ($scope, $uibModalInstance, reportInfo, ReportQuest) {
    $scope.offenderName = reportInfo.offenderName;
    $scope.offenderId = reportInfo.offenderId;
    $scope.reporterId = reportInfo.reporterId;

    $scope.selectedReason = '';
    $scope.reportReasons = ['Bad Offline Behavior', 'Inappropriate Messages', 'Inappropriate Photos',
        'Inappropriate Quest Details', 'Other', 'Spam Or Misleading'];
    $scope.reportMessage = '';
    $scope.errorMakingReport = false;

    $scope.ok = function () {
        $scope.errorMakingReport = false;
        var data = {
            reporterId: $scope.reporterId,
            offenderId: $scope.offenderId,
            reason: $scope.selectedReason,
            message: $scope.reportMessage
        };
        ReportQuest.report(data)
            .success(function(response) {
                $uibModalInstance.close();
            })
            .error(function(response) {
                $scope.errorMakingReport = true;
                console.log(response);
            });
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});

// Factory For Profile Edit Page Function
app.factory('UpdateProfile', ['$http', function ($http) {
    return {
        update: function (data) {
            console.log(JSON.stringify(data));
            return $http({
                method: 'put',
                url: 'http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/user',
                data: JSON.stringify(data),
                headers: {'Content-Type': 'application/json'}
            })
        }
    }
}]);

// Controller For Profile Edit Page
app.controller('QuesterEditCtrl', function ($scope, $http, $cookies, $routeParams, UpdateProfile, $location, Upload) {
    $scope.userId = $routeParams.userId;
    $scope.name = '';
    $scope.email = '';
    $scope.roleId = 'default';
    $scope.profileImageId = '';
    $scope.profileFile = '';
    $scope.cities = [];
    $scope.city = '';
    $scope.cantCreateUser = false;

    $scope.myImage = '';
    $scope.myCroppedImage = '';

    var handleFileSelect = function(evt) {
        var file=evt.currentTarget.files[0];
        var reader = new FileReader();
        reader.onload = function (evt) {
            $scope.$apply(function($scope) {
                $scope.myImage = evt.target.result;
            });
        };
        reader.readAsDataURL(file);
    };
    angular.element(document.querySelector('#fileInput')).on('change', handleFileSelect);

    $http.get("http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/user/" + $scope.userId, {
        transformRequest: angular.identity,
        headers: {
            'Content-Type': undefined
        },
        id: $scope.userId
    })
        .success(function (response) {
            $scope.name = response.name;
            $scope.email = response.email;
        })
        .error(function (response) {
            console.log(response);
        });

    $http.get("http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/city", {
        transformRequest: angular.identity,
        headers: {
            'Content-Type': undefined
        }
    })
        .success(function (response) {
            $scope.cities = response;
        })
        .error(function (response) {
            console.log(response);
        });


    $scope.updateProfile = function (event) {
        console.log(event);
        if ($scope.myCroppedImage !== null && $scope.myCroppedImage !== ''){
            var binary = atob($scope.myCroppedImage.split(',')[1]);
            var mimeString = $scope.myCroppedImage.split(',')[0].split(':')[1].split(';')[0];
            var array = [];
            for(var i = 0; i < binary.length; i++) {
                array.push(binary.charCodeAt(i));
            }
            var blob = new Blob([new Uint8Array(array)], {type: mimeString});

            var theBlob = blob;
            theBlob.lastModifiedDate = new Date();
            theBlob.name = "my-image.png";

            console.log(theBlob);
            Upload.upload({
                url: 'http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/image',
                data: {file: theBlob}
            }).then(function (resp) {
                console.log('Success ' + resp + ' uploaded. Response: ' + resp);
                console.log(resp);
                $scope.profileImageId = resp.data.id;

                var data = {
                    profileImageId: $scope.profileImageId
                };

                if ($scope.name !== '') {
                    data.name = $scope.name;
                }
                if ($scope.email !== '') {
                    data.email = $scope.email;
                }
                $scope.cantCreateUser = false;
                UpdateProfile.update(data)
                    .then(function () {
                        $location.path("/home");
                    }, function (response) {
                        if (response.data.statusCode === 409) {
                            $scope.emailExists = true;
                        }
                        if (response.data.statusCode === 500) {
                            $scope.cantCreateUser = true;
                        }
                    });
            }, function (resp) {
                console.log('Error status: ' + resp.status);
            }, function (evt) {
                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
            });

        }
        else {
            var data = {
            };

            if ($scope.name !== '') {
                data.name = $scope.name;
            }
            if ($scope.email !== '') {
                data.email = $scope.email;
            }
            $scope.cantCreateUser = false;
            UpdateProfile.update(data)
                .then(function () {
                    $location.path("/home");
                }, function (response) {
                    if (response.data.statusCode === 409) {
                        $scope.emailExists = true;
                    }
                    if (response.data.statusCode === 500) {
                        $scope.cantCreateUser = true;
                    }
                });
        }
    };
});

/* Factory for connecting to Sendbird, currently unused
app.factory('ConnectSendbird', [ '$cookies', function ($cookies) {
    return {
        connectSB: function () {
            sb = new SendBird({
                appId: appId
            });
            if ($cookies.get('userId') !== null) {
                userId = $cookies.get('userId') ;
                console.log(userId, accessToken);
                sb.connect(userId, accessToken, function(user, error) {
                    currentUser = user;
                    if (error) {
                        console.log(error);
                    }
                    console.log(user);
                });
                console.log('**********************');
                console.log(sb);
            }
        }
    }
}]);
*/

// Controller For Chat Page
app.controller('ChatCtrl', function ($scope, $http, $cookies) {

    $scope.userId = $cookies.get('userId');
    $scope.email = '';
    $scope.garb = '';
    $scope.chatList = [];

    $scope.chatHeader = 'Chat';
    $scope.chatMessages = [];
    $scope.newMessage = '';
    $scope.addedMessages = [];

    $scope.garb = sb;

    var currentChannel = null;

    var channelListQuery = sb.GroupChannel.createMyGroupChannelListQuery();
    channelListQuery.includeEmpty = true;
    channelListQuery.limit = 20; // pagination limit could be set up to 100

    var been = [0];

    var chats = [];
    console.log(channelListQuery);
    if (channelListQuery.hasNext) {
        channelListQuery.next(function(channelList, error){
            if (error) {
                console.error(error);
                return;
            }
            var numChannels = channelList.length;

            for (var i = 0; i < numChannels; i++) {
                var chatMembers = channelList[i].members;

                var obj = {};
                obj["chatUrl"] = channelList[i].url;
                obj["unreadCount"] = 0;

                if (chatMembers[0].userId !== $scope.userId){
                    obj["chatterId"] = chatMembers[0].userId;
                    obj["chatterNickname"] = chatMembers[0].nickname;
                    obj["chatterStatus"] = chatMembers[0].connectionStatus;
                }
                else {
                    obj["chatterId"] = chatMembers[1].userId;
                    obj["chatterNickname"] = chatMembers[1].nickname;
                    obj["chatterStatus"] = chatMembers[1].connectionStatus;
                }
                chats.push(obj);
                console.log(chats);
                $scope.$apply($scope.chatList.push(obj));


                var ChannelHandler = new sb.ChannelHandler();

                ChannelHandler.onMessageReceived = function(channel, message){
                    var tempMess = message.messageId;
                    if (tempMess !== been[been.length - 1]) {
                        been.push(tempMess);
                        console.log(channel, message);

                        if (channel === currentChannel) {
                            var currMessage = {};
                            currMessage["direction"] = "left";
                            currMessage["sent"] = false;
                            currMessage["photo"] = message._sender.profileUrl;

                            currMessage["sender"] = message._sender.nickname;
                            currMessage["createdAt"] = new Date(message.createdAt);
                            currMessage["messageText"] = message.message;
                            $scope.$apply($scope.chatMessages.push(currMessage));
                        }
                        else {
                            var numChats = $scope.chatList.length;
                            for (var chatNum =0; chatNum < numChats; chatNum++) {
                                if(channel.url === $scope.chatList[chatNum].chatUrl) {
                                    $scope.$apply($scope.chatList[chatNum].unreadCount += 1);
                                }
                            }
                        }
                    }

                };

                sb.addChannelHandler('' + i, ChannelHandler);
            }

        });
    }
    $scope.sendNewMessage = function (event) {
        console.log(event);
        if (currentChannel === null || $scope.newMessage === '') {
            return;
        }

        currentChannel.sendUserMessage($scope.newMessage, '', '', function(message, error){
            if (error) {
                console.error(error);
                return;
            }
            console.log(message);

            var currMessage = {};
            currMessage["direction"] = "right";
            currMessage["sent"] = true;
            currMessage["photo"] = message._sender.profileUrl;

            currMessage["sender"] = message._sender.nickname;
            currMessage["createdAt"] = new Date(message.createdAt);
            currMessage["messageText"] = message.message;

            $scope.$apply($scope.chatMessages.push(currMessage));
        });
        $scope.newMessage = '';
    };

    $scope.selectChat = function(event, chat) {
        sb.GroupChannel.getChannel(chat.chatUrl, function(channel, error) {

            var numChats = $scope.chatList.length;
            for (var chatNum =0; chatNum < numChats; chatNum++) {
                if(channel.url === $scope.chatList[chatNum].chatUrl) {
                    $scope.chatList[chatNum].unreadCount = 0;
                }
            }

            if (error) {
                console.error(error);
                return;
            }

            // Successfully fetched the channel.
            console.log(channel);
            currentChannel = channel;

            var chatMembers = channel.members;

            if (chatMembers[0].userId !== $scope.userId){
                $scope.chatHeader = chatMembers[0].nickname;
            }
            else {
                $scope.chatHeader = chatMembers[1].nickname;
            }

            $scope.chatMessages = [];
            $scope.newMessage = '';

            var messageListQuery = channel.createPreviousMessageListQuery();

            messageListQuery.load(20, true, function(messageList, error){
                if (error) {
                    console.error(error);
                    return;
                }

                messageList = messageList.reverse();
                console.log(messageList);

                var messagesLen = messageList.length;
                for (var j = 0; j < messagesLen; j++) {
                    var currMessage = {};
                    /*
                    direction: left if other user, right if me
                    messageText: content
                     */
                    if(messageList[j]._sender.userId === $scope.userId) {
                        currMessage["direction"] = "right";
                        currMessage["sent"] = true;
                        currMessage["photo"] = messageList[j]._sender.profileUrl;
                    } else {
                        currMessage["direction"] = "left";
                        currMessage["sent"] = false;
                        currMessage["photo"] = messageList[j]._sender.profileUrl;
                    }
                    currMessage["sender"] = messageList[j]._sender.nickname;
                    currMessage["createdAt"] = new Date(messageList[j].createdAt);
                    currMessage["messageText"] = messageList[j].message;

                    $scope.$apply($scope.chatMessages.push(currMessage));
                }

            });
        });
    };

});

// Controller For Home Page
app.controller('HomeCtrl', function ($scope, $http, $cookies) {
    $scope.questFindIds = [];
    $scope.questsFind = [];

    $scope.questsActiveIds = [];
    $scope.questsActive = [];

    $scope.questCreatedIds = [];
    $scope.questsCreated = [];

    $scope.questIdsToTag = {};
    $scope.tags = {};

    $http.get("http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/tag", {
        transformRequest: angular.identity,
        headers: {
            'Content-Type': undefined
        }
    })
        .success( function (response) {
            var numTags = response.length;
            for (var j = 0; j < numTags; j++) {
                $scope.tags[response[j].tagId] = response[j].tagName;
            }
        })
        .error( function (response) {

        })
        .then(function () {
            $http.get("http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/quest", {
                transformRequest: angular.identity,
                headers: {
                    'Content-Type': undefined
                }
            })
                .success(function (response) {
                    console.log(response);
                    var allQuests = response;
                    var arrLength = allQuests.length;
                    for (var i = 0; i < arrLength; i++) {
                        if (allQuests[i].giverId !== $cookies.get('userId') && allQuests[i].receiverId === null) {
                            $scope.questFindIds.push(allQuests[i].questId);
                            if (allQuests[i].tags.length > 0) {
                                $scope.questIdsToTag[allQuests[i].questId] = $scope.tags[allQuests[i].tags[0]];
                            } else {
                                $scope.questIdsToTag[allQuests[i].questId] = '';
                            }
                        }
                        else if(allQuests[i].giverId === $cookies.get('userId')) {
                            $scope.questCreatedIds.push(allQuests[i].questId);
                            if (allQuests[i].tags.length > 0) {
                                $scope.questIdsToTag[allQuests[i].questId] = $scope.tags[allQuests[i].tags[0]];
                            } else {
                                $scope.questIdsToTag[allQuests[i].questId] = '';
                            }
                        }
                        else if(allQuests[i].receiverId === $cookies.get('userId')) {
                            $scope.questsActiveIds.push(allQuests[i].questId);
                            if (allQuests[i].tags.length > 0) {
                                $scope.questIdsToTag[allQuests[i].questId] = $scope.tags[allQuests[i].tags[0]];
                            } else {
                                $scope.questIdsToTag[allQuests[i].questId] = '';
                            }
                        }
                    }
                    console.log(response);
                })
                .error(function (response) {
                    console.log(response);
                })
                .then(function () {
                    var idLength = $scope.questFindIds.length;
                    if (idLength > 4) {
                        idLength = 4;
                    }
                    for (var i = 0; i < idLength; i++) {
                        $http.get("http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/quest/" + $scope.questFindIds[i], {
                            transformRequest: angular.identity,
                            headers: {
                                'Content-Type': undefined
                            }
                        })
                            .success(function (response) {
                                $scope.questsFind.push({
                                    questId: response.questId,
                                    created: response.created,
                                    name: response.name
                                });
                            })
                            .error(function (response) {
                                console.log(response);
                            });
                    }

                    idLength = $scope.questsActiveIds.length;
                    if (idLength > 4) {
                        idLength = 4;
                    }
                    for (i = 0; i < idLength; i++) {
                        $http.get("http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/quest/" + $scope.questsActiveIds[i], {
                            transformRequest: angular.identity,
                            headers: {
                                'Content-Type': undefined
                            }
                        })
                            .success(function (response) {
                                $scope.questsActive.push({
                                    questId: response.questId,
                                    created: response.created,
                                    name: response.name
                                });
                            })
                            .error(function (response) {
                                console.log(response);
                            })
                    }

                    idLength = $scope.questCreatedIds.length;
                    if (idLength > 3) {
                        idLength = 3;
                    }
                    for (i = 0; i < idLength; i++) {
                        $http.get("http://ec2-54-149-169-90.us-west-2.compute.amazonaws.com:3000/quest/" + $scope.questCreatedIds[i], {
                            transformRequest: angular.identity,
                            headers: {
                                'Content-Type': undefined
                            }
                        })
                            .success(function (response) {
                                $scope.questsCreated.push({
                                    questId: response.questId,
                                    created: response.created,
                                    name: response.name
                                });
                            })
                            .error(function (response) {
                                console.log(response);
                            })
                    }
                });
        });
});

// Factory For User Authorization Throughout App
app.factory('authorizationInterceptor', function ($rootScope, $q, $window, $cookies) {
    return {
        request: function (config) {
            config.headers = config.headers || {};
            console.log(config);
            if ($cookies.get('jwt')) {
                if (!config.url.includes('google')) {
                    config.headers.authorization = 'Bearer ' + $cookies.get('jwt');
                }
            }
            return config;
        },
        response: function (response) {
            if (response.status === 401) {
                // this user is not authenticated
            }
            return response || $q.when(response);
        }
    };
});

// Configuring User Authorization Interceptor
app.config(function ($httpProvider) {
    $httpProvider.interceptors.push('authorizationInterceptor');
});
